import nltk
from evaluation import evaluate
# [example]
# hypothesis = ['It', 'is', 'a', 'cat', 'at', 'room']
# reference1 = ['It', 'is', 'a', 'cat', 'at', 'room']
# reference = ['It', 'is', 'a', 'cat', 'inside', 'the', 'room']
# #there may be several references
# BLEUscore = nltk.translate.bleu_score.sentence_bleu([reference, reference1], hypothesis)
# print(BLEUscore)


# [add evaluate bleu score]

def bleu(data_val, image_captions, max_length, attention_features_shape, decoder, image_features_extract_model, encoder, tokenizer):
    bleus = 0
    count = 1
    for labels, img_paths in data_val:
        for label, img_path in zip(labels, img_paths):
            # reference = all labels
            path = img_path.numpy().decode("utf-8")
            reference = image_captions[path]
            #result, _ = evaluate(img_path)
            result, _ = evaluate(img_path, max_length, attention_features_shape, decoder, image_features_extract_model, encoder,tokenizer)
            bleus += nltk.translate.bleu_score.sentence_bleu(reference, result)
            count += 1
    bleu = bleus / count
    return bleu