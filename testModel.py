from models.attentionmechanism import *
# You'll generate plots of attention in order to see which parts of an image
# our model focuses on during captioning
import matplotlib.pyplot as plt

# Scikit-learn includes many helpful utilities

import numpy as np
from PIL import Image


# parameters
# turn on extract feature funtion with extract_feature = 1
extract_feature = 0
top_k = 75
# Shape of the vector extracted from InceptionV3 is (64, 2048)
# These two variables represent that vector shape
features_shape = 2048
attention_features_shape = 64

BATCH_SIZE = 1
BUFFER_SIZE = 1000
embedding_dim = 256
units = 512

tokenizerPath = 'saveTokenizer/tokenizer.pickle'
tokenizer = load_tokenizer(tokenizerPath=tokenizerPath)
image_features_extract_model = load_backbone()
max_length = 16

vocab_size = len(tokenizer.word_index) + 1

encoder = CNN_Encoder(embedding_dim)
decoder = RNN_Decoder(embedding_dim, units, vocab_size)

try:
    encoder.load_weights('pretrainedModel/best-encoder')
    decoder.load_weights('pretrainedModel/best-decoder')
    print('load_weight success')
except Exception as e:
    print('load fall')
    print(e)




def evaluate(image):
    attention_plot = np.zeros((max_length, attention_features_shape))

    hidden = decoder.reset_state(batch_size=1)

    temp_input = tf.expand_dims(load_image(image)[0], 0)
    img_tensor_val = image_features_extract_model(temp_input)
    img_tensor_val = tf.reshape(img_tensor_val, (img_tensor_val.shape[0], -1, img_tensor_val.shape[3]))

    features = encoder(img_tensor_val)

    dec_input = tf.expand_dims([tokenizer.word_index['<start>']], 0)
    result = []

    for i in range(max_length):
        predictions, hidden, attention_weights = decoder(dec_input, features, hidden)

        attention_plot[i] = tf.reshape(attention_weights, (-1, )).numpy()
        # bang voi tf.argmax()
        #predicted_id = tf.random.categorical(predictions, 1)[0][0].numpy()
        predicted_id = tf.argmax(predictions, axis=-1).numpy()[0]
        try:
            prepare = tokenizer.index_word[predicted_id] == '<end>'
        except:
            prepare = False
        if prepare:
            return result, attention_plot

        result.append(tokenizer.index_word[predicted_id])
        dec_input = tf.expand_dims([predicted_id], 0)

    attention_plot = attention_plot[:len(result), :]
    return result, attention_plot


def plot_attention(image, result, attention_plot):
    temp_image = np.array(Image.open(image))

    fig = plt.figure(figsize=(10, 10))

    len_result = len(result)
    for l in range(len_result):
        temp_att = np.resize(attention_plot[l], (8, 8))
        # add_subplot(nrows, ncols, index, **kwargs)
        ax = fig.add_subplot(len_result//2, len_result//2, l+1)
        ax.set_title(result[l])
        img = ax.imshow(temp_image)
        ax.imshow(temp_att, cmap='gray', alpha=0.6, extent=img.get_extent())

    plt.tight_layout()
    plt.show()

#[Test image captioning]

# result, attention_plot = evaluate('train2014/COCO_train2014_000000001518.jpg')
# print('result: ', result)
# plot_attention('train2014/COCO_train2014_000000001518.jpg', result, attention_plot)

