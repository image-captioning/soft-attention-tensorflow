import tensorflow as tf

from sklearn.utils import shuffle
import numpy as np
import os
import json
import pickle


# load and preprocess image for inception_v3
def load_image(image_path):
    img = tf.io.read_file(image_path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, (299, 299))
    img = tf.keras.applications.inception_v3.preprocess_input(img)
    return img, image_path


def load_backbone():
    # load pretrained backbone model
    image_model = tf.keras.applications.InceptionV3(include_top=False,
                                                    weights='imagenet')
    new_input = image_model.input
    hidden_layer = image_model.layers[-1].output

    image_features_extract_model = tf.keras.Model(new_input, hidden_layer)
    return image_features_extract_model


def get_captions_and_images_link(annotation_file, PATH, num_examples=30000):
    with open(annotation_file, 'r') as f:
        annotations = json.load(f)

    all_captions = []
    all_img_name_vector = []

    for annot in annotations['annotations']:
        caption = '<start> ' + annot['caption'] + ' <end>'
        image_id = annot['image_id']
        full_coco_image_path = PATH + 'COCO_train2014_' + '%012d.jpg' % (image_id)

        all_img_name_vector.append(full_coco_image_path)
        all_captions.append(caption)

    # Shuffle captions and image_names together
    # Set a random state
    train_captions, img_name_vector = shuffle(all_captions,
                                              all_img_name_vector,
                                              random_state=1)

    if num_examples:
        train_captions = train_captions[:num_examples]
        img_name_vector = img_name_vector[:num_examples]
    return train_captions, img_name_vector


# get_sub_data theo image minh dang co
def get_sub_data(img_name_vector, train_captions):
    files = os.listdir('train2014')
    list_index = []
    for file in files:
        file = 'train2014/' + file
        # index = img_name_vector.index(file)

        #values = np.array(img_name_vector)
        indexs = np.where(img_name_vector == file)[0]
        # list_index.append(indexs)
        list_index = np.concatenate((list_index, indexs), axis=0)

    list_index = np.array(list_index).astype(int)
    sub_train_captions = [train_captions[i] for i in list_index]
    sub_img_name_vector = [img_name_vector[j] for j in list_index]
    return sub_train_captions, sub_img_name_vector


def extract_feature_vector(img_name_vector, image_features_extract_model, BATCH_FEATURE_EXTRACTION):
    # get feature map
    encode_train = sorted(set(img_name_vector))
    image_dataset = tf.data.Dataset.from_tensor_slices(encode_train)

    num_of_image = len(encode_train)

    # set batch and preprocess image
    image_dataset = image_dataset.map(load_image, num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(BATCH_FEATURE_EXTRACTION)
    index = 1
    for img, path in image_dataset:
        # (bs, 8x8x2048)
        batch_features = image_features_extract_model(img)
        batch_features = tf.reshape(batch_features,
                                    (batch_features.shape[0], -1, batch_features.shape[3]))  # bs, 64, 2048

        for bf, p in zip(batch_features, path):
            path_of_feature = 'feature_extractor/' + p.numpy().decode("utf-8")
            np.save(path_of_feature, bf.numpy())
        print("processing image feature: "+ str(index/(num_of_image/img.shape[0])*100) + "%")
        index += 1


# Find the maximum length of any caption in our dataset
def calc_max_length(tensor):
    return max(len(t) for t in tensor)


def load_tokenizer(tokenizerPath):
    with open(tokenizerPath, 'rb') as handle:
        tokenizer = pickle.load(handle)
    return tokenizer


def text_processing(train_captions, tokenizerPath, generate_tokenizer=1, top_k=20000):
    if generate_tokenizer:
        tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=top_k,
                                                          oov_token="<unk>",
                                                          filters='!"#$%&()*+.,-/:;=?@[\]^_`{|}~ ')
        tokenizer.fit_on_texts(train_captions)

        train_seqs = tokenizer.texts_to_sequences(train_captions)

        tokenizer.word_index['<pad>'] = 0
        tokenizer.index_word[0] = '<pad>'

        with open(tokenizerPath, 'wb') as handle:
            pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        with open(tokenizerPath, 'rb') as handle:
            tokenizer = pickle.load(handle)

    # Create the tokenized vectors
    train_seqs = tokenizer.texts_to_sequences(train_captions)

    # Pad each vector to the max_length of the captions
    # If you do not provide a max_length value, pad_sequences calculates it automatically
    cap_vector = tf.keras.preprocessing.sequence.pad_sequences(train_seqs, padding='post')
    max_length = calc_max_length(train_seqs)

    return tokenizer, cap_vector, max_length


# Load the numpy files
def map_func_val(img_name, cap, image_path):
    img_tensor = np.load('feature_extractor/'+ img_name.decode('utf-8')+'.npy')
    return img_tensor, cap, image_path

def map_func(img_name, cap):
    img_tensor = np.load('feature_extractor/'+ img_name.decode('utf-8')+'.npy')
    return img_tensor, cap

# traning dataset
def make_val_dataset(img_name_train, cap_train, BUFFER_SIZE, BATCH_SIZE):
    dataset = tf.data.Dataset.from_tensor_slices((cap_train, img_name_train))
    # Use map to load the numpy files in parallel
    # dataset = dataset.map(lambda item2, item3: tf.numpy_function(
    #     map_func_val, [item1, item2, item3], [tf.float32, tf.int32, tf.string]),
    #                       num_parallel_calls=tf.data.experimental.AUTOTUNE)

    dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset


def make_training_dataset(imgs_name, caps, BUFFER_SIZE, BATCH_SIZE):
    dataset = tf.data.Dataset.from_tensor_slices((imgs_name, caps))
    # Use map to load the numpy files in parallel
    dataset = dataset.map(lambda item1, item2: tf.numpy_function(
        map_func, [item1, item2], [tf.float32, tf.int32]),
                          num_parallel_calls=tf.data.experimental.AUTOTUNE)

    dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset


# get {image: [captions,...]}
def get_all_captions_for_one_image(all_img, all_caption):
    tuple = {}
    for i, img in enumerate(all_img):
        if img not in tuple:
            tuple[img] = []
        tuple[img].append(all_caption[i].split()[1:-1])
    return tuple

