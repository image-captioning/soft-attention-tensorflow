import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
import nltk
import tensorflow as tf
import pandas
from evaluationMetrics.bleuScore import bleu
from utils import *
from models.attentionmechanism import *
import matplotlib.pyplot as plt

# Scikit-learn includes many helpful utilities
from sklearn.model_selection import train_test_split
import time
import json



# GPU = "/GPU:0"
print('runcode')
# parameters
# turn on extract feature funtion with extract_feature = 1
extract_feature = 1
generate_tokenizer = 1
get_check_point = 0


tokenizerPath = 'saveTokenizer/tokenizer.pickle'

# Shape of the vector extracted from InceptionV3 is (64, 2048)
# These two variables represent that vector shape
features_shape = 2048
attention_features_shape = 64
BATCH_FEATURE_EXTRACTION = 64
BATCH_SIZE = 64
BUFFER_SIZE = 1000
embedding_dim = 256
units = 512
# xem xet tokenizer vocab bao nhieu.
top_k = 20000
# vocab_size = top_k + 1


annotation_file = 'annotations/captions_train2014.json'
PATH = 'train2014/'

# Read the json file
with open(annotation_file, 'r') as f:
    annotations = json.load(f)

# Store captions and image names in vectors

# backbone
image_features_extract_model = load_backbone()
# get captions, img_path in file captions train
all_train_captions, all_img_name_vector = get_captions_and_images_link(annotation_file, PATH, None)
# get only file if image exist in folder image
train_captions, img_name_vector = get_sub_data(np.array(all_img_name_vector), all_train_captions)


if extract_feature:
    # [TURN ON WHEN EXTRACT FEATURE MAP]extract feature map (bs, 64, 2048) and save in feature_extractor/train2014
    print("[infor] extract feature image")
    extract_feature_vector(img_name_vector, image_features_extract_model, BATCH_FEATURE_EXTRACTION)


# text processing
tokenizer, cap_vector, max_length = text_processing(train_captions, tokenizerPath, generate_tokenizer, top_k)

vocab_size = len(tokenizer.word_index) + 1
#vocab_size = top_k + 1
print("len vocab: ", vocab_size)
# Create training and validation sets using an 80-20 split
img_name_train, img_name_val, cap_train, cap_val = train_test_split(img_name_vector,
                                                                    cap_vector,
                                                                    test_size=0.2,
                                                                    random_state=0)



data_val = make_val_dataset(img_name_val, cap_val, BUFFER_SIZE, BATCH_SIZE)
dataset = make_training_dataset(img_name_train, cap_train, BUFFER_SIZE, BATCH_SIZE)
image_captions = get_all_captions_for_one_image(all_img_name_vector, all_train_captions)



# vocab_size = len(tokenizer.index_word) + 1
encoder = CNN_Encoder(embedding_dim)
decoder = RNN_Decoder(embedding_dim, units, vocab_size)

optimizer = tf.keras.optimizers.Adam()
# from_logits=True auto add softmax in output
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')


def loss_function(real, pred):
    mask = tf.math.logical_not(tf.math.equal(real, 0))
    loss_ = loss_object(real, pred)
    mask = tf.cast(mask, dtype=loss_.dtype)
    # avoid positions value = 0 in real labels
    loss_ *= mask
    # mean all element
    return tf.reduce_mean(loss_)

# checkpoint defination
checkpoint_path = "./checkpoints/train"
ckpt = tf.train.Checkpoint(encoder=encoder,
                           decoder=decoder,
                           optimizer=optimizer)
ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=5)
# load checkpoint
if get_check_point:
    try:
        print("[INFO] Load checkpoint")
        if ckpt_manager.latest_checkpoint:
            start_epoch = int(ckpt_manager.latest_checkpoint.split('-')[-1])
            # restoring the latest checkpoint in checkpoint_path
            ckpt.restore(ckpt_manager.latest_checkpoint)
    except:
        print('[INFO] Cant load checkpoint')
else:
    start_epoch = 0

loss_plot = []


# train for 1 batch
def train_step(img_tensor, target):
    loss = 0
    # initializing the hidden state for each batch
    # because the captions are not related from image to image
    hidden = decoder.reset_state(batch_size=target.shape[0])

    dec_input = tf.expand_dims([tokenizer.word_index['<start>']] * target.shape[0], 1)

    with tf.GradientTape() as tape:
        features = encoder(img_tensor)

        for i in range(1, target.shape[1]):
            # passing the features through the decoder
            predictions, hidden, _ = decoder(dec_input, features, hidden)
            # loss of batch
            loss += loss_function(target[:, i], predictions)

            # using teacher forcing
            dec_input = tf.expand_dims(target[:, i], 1)

    total_loss = (loss / int(target.shape[1]))

    trainable_variables = encoder.trainable_variables + decoder.trainable_variables

    gradients = tape.gradient(loss, trainable_variables)

    optimizer.apply_gradients(zip(gradients, trainable_variables))
    # loss is loss of batch, total_loss is loss mean. evaluate via total_loss
    return loss, total_loss


EPOCHS = 2000

num_steps = len(img_name_train) // BATCH_SIZE
early_stop_check = 0
best_loss = 100
patience = 15
wait = 0
check_end = 0
# with open('losses.txt', 'w') as losses_file:

data_frame_loss_bleu = pandas.DataFrame(columns=['epoch','loss','bleu'])
n_df = 0

# with tf.device('/device:GPU:1'):

for epoch in range(start_epoch, EPOCHS):
    start = time.time()
    # loss in a epoch
    total_loss = 0

    for (batch, (img_tensor, target)) in enumerate(dataset):
        batch_loss, t_loss = train_step(img_tensor, target)
        # use t_loss for valuate loss
        total_loss += t_loss

        if batch % 100 == 0:
            print('Epoch {} Batch {} Loss {:.4f}'.format(
                epoch + 1, batch, batch_loss.numpy() / int(target.shape[1])))

    # storing the epoch end loss value to plot later
    loss_now = total_loss / num_steps
    loss_plot.append(loss_now)
    # write loss and epoch to file


    # early stopping
    if loss_now < best_loss:
        best_loss = loss_now
        wait = 0
    else:
        wait += 1
        if wait > patience:
            check_end = 1
            # for item in loss_plot:
            #     losses_file.write("%.3f - %.3f\n" % (item, epoch + 1))
            break

    # bleu score( dang chay one image, can modifine lai thanh batch se nhanh hon)
    print("[INFO] Evaluate with BLEU SCORE")

    #bleu_score = bleu(data_val, image_captions)
    #bleu_score = 1
    bleu_score = bleu(data_val, image_captions, max_length, attention_features_shape, decoder, image_features_extract_model, encoder, tokenizer)
    print("Epoch {} BLEU: {}".format(epoch+1, bleu_score))


    # save model and checkpoint
    if epoch % 5 == 0:
        ckpt_manager.save()
        if (total_loss / num_steps) <= min(loss_plot):
            encoder.save_weights("pretrainedModel/best-encoder")
            decoder.save_weights("pretrainedModel/best-decoder")

    # epoch_loss_bleu = {
    #     "epoch": epoch + 1,
    #     "loss": loss_now,
    #     "bleu": bleu_score
    # }
    data_frame_loss_bleu.at[n_df, "epoch"] = epoch + 1
    data_frame_loss_bleu.at[n_df, "loss"] = loss_now.numpy()
    data_frame_loss_bleu.at[n_df, "bleu"] = bleu_score
    data_frame_loss_bleu.to_csv('log.csv', encoding='utf-8')
    n_df += 1
    #epoch_loss_bleu_list.append(epoch_loss_bleu)

    # string_dump = "batch loss: %.3f - epoch: %.3f - BLEU: %.3f\n" % (loss_now, epoch + 1, bleu_score)
    # losses_file.write(string_dump)

    #losses_file.write("batch loss: %.3f - epoch: %.3f - BLEU: %.3f\n" % (loss_now, epoch + 1, bleu_score))
    print('Epoch {} Total Loss {:.6f}'.format(epoch + 1, total_loss/num_steps))
    print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))


print('finish')





