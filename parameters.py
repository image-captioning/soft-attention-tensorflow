import json

data = {
    'a':10,
    'b': 1
}
# Serialize data into file:
json.dump(data, open("file_name.json", 'w'))

# Read data from file:
data = json.load(open("file_name.json") )

print(data)